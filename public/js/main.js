var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");

window.onload = function () {
    
    var n = $('.select__element').first();
    var t = 0;
    var can = true;
    var cnt = $('.select__element').length;
    setInterval(function(){
     if( can )
     {
         n.removeClass('zoommed');
         n.children().removeClass('zoommed-s');
        
         if(t == cnt){
             n = $('.select__element').first();
             t = 0;
         }
         if( t > 0 ){
             n = n.next();
         }
         n.addClass('zoommed');
         n.children().addClass('zoommed-s');
         t++;
     }
    }, 1000);
    $('.select__element').hover(function(){
     can = false;
     $('.select__element').removeClass('zoommed');
     $('.select__element').children().removeClass('zoommed-s');
     $(this).addClass('zoommed');
     $(this).children().addClass('zoommed-s');
    }, function(){
     $('.select__element').removeClass('zoommed');
     $('.select__element').children().removeClass('zoommed-s');
    });

    $('.select__element').on('mouseleave', function(){
        can = true;
    });

    
    /** навигация */
    $(".nav-activator").click(function () {
        var fireCount = -1;
        var mMenuH = $(".menu-mobile__wrap").css("display") == "none" ? 0 : $(".site-header").height();
        $('html, body').animate({
            scrollTop: $(this.dataset.to).offset().top - mMenuH,
        }, {
            duration: 1200,
            easing: "swing",
            complete: hideMobM
        });
        function hideMobM() {
            fireCount++;
            console.log(fireCount);
            if (!fireCount)
                return;
            if (mMenuH)
                $(".hamburger").click();
        }
    });
    /** мобильное меню открыть/закрыть */
    $(".hamburger").click(function () {
        this.classList.toggle("is-active");
        document.querySelector(".menu-wrap").classList.toggle("active");
    });
    /** massonry */
    $('.our-project__section .content__box').isotope({
        //gutter: $(".gutter-sizer")[0],
        //resize: false,
        itemSelector: '.column__element',
        transitionDuration: 0,
        masonry: {
            columnWidth: '.grid-sizer',
        }
    });
    /** усечение текста */
    $(".project-text__wrap").dotdotdot({
        after: "span.arrow",
        watch: true
    });
    /** управление попапом */
    $(".bg-popup, .popup .exit").click(function () {
        $(".popup__container").fadeOut({
            duration: "slow",
            complete: function () {
                //document.body.style.overflow = "";
            }
        });
        if ($(".menu-mobile__wrap").css("display") !== "none")
            $("header").fadeIn();
    });
    $('body').on('click', ".popup-btn", function () {
        $(".popup__container").fadeIn({
            duration: "slow",
            start: function () {
                //document.body.style.overflow = "hidden";
            }
        });
        var nextN = this.dataset.bind;
        changeSlide.call(document.querySelector(".popup a[data-bind=" + nextN + "]"));
        if ($(".menu-mobile__wrap").css("display") == "none")
            $("header").fadeIn();
    });
    /** упрвление мобильным попапом */
    $(".swipe-arrow").click(function (e) {
        $(".center-menu__bar").fadeIn({
            duration: "slow",
        });
        $("header").fadeOut();
        //document.addEventListener('touchstart', touchstart);
        //document.addEventListener('touchmove', touchmove);
    });
    $(".center-menu__bar").click(function (e) {
        $(".center-menu__bar").fadeOut({
            duration: "slow",
        });
        if (e.target.classList.contains("center-menu__bar"))
            $("header").fadeIn();
        //document.removeEventListener('touchstart', touchstart);
        //document.removeEventListener('touchmove', touchmove);
    });
    function touchstart(e) {
        e.preventDefault();
    }
    function touchmove(e) {
        e.preventDefault();
    }
    /** попап слайдер */
    $(".popup .nav__wrap a").click(changeSlide);
    function changeSlide() {
        var curRef = document.querySelector(".popup .nav__wrap .active");
        var nextN = this.dataset.bind;
        var btnActive = this.dataset.btn;
        if (btnActive === "non-active") {
            document.querySelector(".popup__container .content__box a").style.display = "none";
        }
        else {
            document.querySelector(".popup__container .content__box a").style.display = "";
        }
        if (curRef) {
            var curSlideN = curRef.dataset.bind;
            $(".popup .img__container[data-bind=" + curSlideN + "]").fadeOut({
                duration: "slow",
                complete: function () {
                }
            });
            curRef.classList.remove("active");
        }
        this.classList.add("active");
        $(".popup .img__container[data-bind=" + nextN + "]").fadeIn({
            duration: "slow",
            complete: function () {
            }
        });
        txtSlideContainer.slick("slickGoTo", $(".popup .slider-text__container p[data-bind=" + nextN + "]").index());
    }
    var speed = 500;
    console.log($('.popup .slider-text__container'))
    // var txtSlideContainer = $('.popup .slider-text__container').slick({
    //     arrows: false,
    //     speed: speed,
    //     fade: true,
    //     draggable: false,
    //     mobileFirst: true
    // }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    //     var a = document.querySelector(".popup__container .content__box a");
    //     //console.log(currentSlide, nextSlide)
    //     var hrefArr = JSON.parse(a.dataset.href);
    //     a.href = hrefArr[nextSlide];
    // }).on('setPosition', function () {
    //     //console.log("resize mthf");
    //     rid++;
    // });
    var rid = 0;
    $(window).on('orientationchange', function () {
        //$(".popup").hide();
        setTimeout(function () {
        }, 200);
    });
    /** стики */
    $(".left-menu__bar").stick_in_parent({
        offset_top: 20,
        //bottoming: false
        sticky_class: "is_sticky",
        recalc_every: true
    });
    $(".swipe__helper").stick_in_parent({
        recalc_every: true
    });
    /** form control */
    $('*[type=tel]').inputmask("+7 (999) 999-99-99");
    $("form input").blur(checker);
    function checker() {
        if (!this.required && !this.value)
            return;
        switch (this.type) {
            case "tel":
                isValidTel(this.value) ? validAct(this) : errorAct(this);
                break;
            case "email":
                isValidEmail(this.value) ? validAct(this) : errorAct(this);
                break;
            default:
                this.value ? validAct(this) : errorAct(this);
        }
    }
    function validAct(el) {
        el.classList.remove("error");
        el.classList.add("valid");
        el.onkeyup = "";
    }
    function errorAct(el) {
        el.classList.remove("valid");
        el.classList.add("error");
        el.onkeyup = checker;
    }
    function isValidTel(tel) {
        var pattern = /\+\d \(\d{3}\) \d{3}-\d{2}-\d{2}/i;
        return pattern.test(tel);
    }
    function isValidEmail(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }
    /** убрать попап */
    $(".popup__container").css("display", "none");
    /** Убрать прелоадер */
    console.log('asdf')
    $(".loader-section.section-left").on("transitionend", function () {
        console.log("end");
        /** Инициализация анимации при intoView */
        AOS.init({
            disable: window.innerWidth < 1024,
            duration: 600,
        });
    });
    $('body').addClass('loaded');
    $('body').css('overflow', 'visible');
};
// отправка формы
$('form').each(function() {
        var form = $(this);
        form.find('.send-form').click(function() {
            form.find('.err').removeClass('err');
            var send = true;

            form.find('.req').each(function() {
                var reqVal = $(this).val();
                if(!reqVal) {
                    
                    send = false;
                }
            });
            if (send === false) {
                alert('Заполните поле!')
            }
            
            if (send === true) {
                $.ajax({
                    cache: false,
                    url: "./send.php",
                    type: "POST",
                    data: form.serialize(),
                    success: function(response){
                        form.trigger('reset')
                        alert('Успешно отправлено!')
                    },
                });
            }
            return false;
        });
    });

function ajaxComplete(res) {
    console.log("sending status: ", res.statusText);
}
function ajaxError(res) {
    console.log("sending rejected");
}
/******************************* */
/****** */
// var map;
// function initMap() {
//     map = new google.maps.Map(document.getElementById('map'), {
//         zoom: 13,
//         center: new google.maps.LatLng(55.745303, 37.626085),
//         scrollwheel: false,
//         styles: [
//             {
//                 "elementType": "geometry",
//                 "stylers": [
//                     {
//                         "color": "#f5f5f5"
//                     }
//                 ]
//             },
//             {
//                 "elementType": "labels.icon",
//                 "stylers": [
//                     {
//                         "visibility": "off"
//                     }
//                 ]
//             },
//             {
//                 "elementType": "labels.text.fill",
//                 "stylers": [
//                     {
//                         "color": "#616161"
//                     }
//                 ]
//             },
//             {
//                 "elementType": "labels.text.stroke",
//                 "stylers": [
//                     {
//                         "color": "#f5f5f5"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "administrative.land_parcel",
//                 "elementType": "labels.text.fill",
//                 "stylers": [
//                     {
//                         "color": "#bdbdbd"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "poi",
//                 "elementType": "geometry",
//                 "stylers": [
//                     {
//                         "color": "#eeeeee"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "poi",
//                 "elementType": "labels.text.fill",
//                 "stylers": [
//                     {
//                         "color": "#757575"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "poi.park",
//                 "elementType": "geometry",
//                 "stylers": [
//                     {
//                         "color": "#e5e5e5"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "poi.park",
//                 "elementType": "labels.text.fill",
//                 "stylers": [
//                     {
//                         "color": "#9e9e9e"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "road.arterial",
//                 "elementType": "geometry",
//                 "stylers": [
//                     {
//                         "color": "#ffffff"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "road.arterial",
//                 "elementType": "labels.text.fill",
//                 "stylers": [
//                     {
//                         "color": "#757575"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "road.highway",
//                 "elementType": "geometry",
//                 "stylers": [
//                     {
//                         "color": "#d7d7d7"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "road.highway",
//                 "elementType": "labels.text.fill",
//                 "stylers": [
//                     {
//                         "color": "#616161"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "road.local",
//                 "elementType": "geometry",
//                 "stylers": [
//                     {
//                         "color": "#ffffff"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "road.local",
//                 "elementType": "labels.text.fill",
//                 "stylers": [
//                     {
//                         "color": "#9e9e9e"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "transit.station",
//                 "elementType": "geometry",
//                 "stylers": [
//                     {
//                         "color": "#eeeeee"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "water",
//                 "elementType": "geometry",
//                 "stylers": [
//                     {
//                         "color": "#a0a09a"
//                     }
//                 ]
//             },
//             {
//                 "featureType": "water",
//                 "elementType": "labels.text.fill",
//                 "stylers": [
//                     {
//                         "color": "#9e9e9e"
//                     }
//                 ]
//             }
//         ]
//     });
//     // var icons = {
//     //     zamoskvoreche: {
//     //         icon: 'img/rz64.jpg'
//     //         //icon: 'img/marker1.png'
//     //     },
//     //     aresidence: {
//     //         icon: 'img/a-r64.jpg'
//     //         //icon: 'img/marker1.png'
//     //     },
//     //     polynka: {
//     //         icon: 'img/44.jpg'
//     //         //icon: 'img/marker1.png'
//     //     },
//     //     ordynka25: {
//     //         icon: 'img/o25.jpg'
//     //         //icon: 'img/marker2.png'
//     //     },
//     //     imperiale: {
//     //         // icon: 'img/marker4.png'
//     //         icon: 'img/pallazo.png'
//     //     },
//     //     soffi: {
//     //         //icon: 'img/marker5.png'
//     //         icon: 'img/sofiya.jpg'
//     //     },
//     //     ordynka19: {
//     //         //icon: 'img/marker6.png'
//     //         icon: 'img/m19.jpg'
//     //     },
//     //     barkli: {
//     //         //icon: 'img/marker7.png'
//     //         icon: 'img/bg.jpg'
//     //     },
//     //     // cloud9: {
//     //     //     //icon: 'img/marker8.png'
//     //     //     icon: 'img/nine.jpg'
//     //     // }
//     // };
//     // var features = [
//     //     {
//     //         position: new google.maps.LatLng(55.7295342, 37.6292782),
//     //         type: 'zamoskvoreche'
//     //     },
//     //     {
//     //         position: new google.maps.LatLng(55.7374319, 37.639578),
//     //         type: 'aresidence'
//     //     },
//     //     {
//     //         position: new google.maps.LatLng(55.7325741, 37.6174272),
//     //         type: 'polynka'
//     //     },
//     //     {
//     //         position: new google.maps.LatLng(55.735632, 37.6236423),
//     //         type: 'ordynka25'
//     //     },
//     //     {
//     //         position: new google.maps.LatLng(55.7344388, 37.6205872),
//     //         type: 'imperiale'
//     //     },
//     //     {
//     //         position: new google.maps.LatLng(55.7468006, 37.6214022),
//     //         type: 'soffi'
//     //     },
//     //     {
//     //         position: new google.maps.LatLng(55.7371273, 37.6235903),
//     //         type: 'ordynka19'
//     //     },
//     //     {
//     //         position: new google.maps.LatLng(55.74116, 37.6207523),
//     //         type: 'barkli'
//     //     },
//     //     {
//     //         position: new google.maps.LatLng(55.7404618, 37.6148843),
//     //         type: 'cloud9'
//     //     }
//     // ];
//     // // Create markers.
//     // features.forEach(function (feature) {
//     //     var marker = new google.maps.Marker({
//     //         position: feature.position,
//     //         icon: icons[feature.type].icon,
//     //         map: map
//     //     });
//     // });
// }


}